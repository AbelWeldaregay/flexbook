# Flexbook

## Introduction:

This report will describe the application FlexBook. FlexBook is a social media solution that facilitates communication between businesses and consumers by providing businesses and organizations an efficient platform to advertise their services while simultaneously giving consumers the ability to easily find the nearest business providing the service. This report will go over the market’s current need for an application like FlexBook, a list of implemented features, design decisions that informed them, any external libraries or APIs that are used by the application a discussion on the potential future of FlexBook, and finally, lessons learned by the team members during development.

## Application Definition and Related Apps:

FlexBook is a social media application solution that is designed to combine the best features of social media applications such as Instagram and Facebook with the best features of business directory services such as Yelp to provide businesses and organizations a platform to advertise their services in a localized area while simultaneously providing consumers the ability to easily find the nearest business providing the service they are looking for.

## Market Need and Target Audience:

There is currently no social media application that provides an efficient & facilitated method of communication between consumers and businesses. Local search services such as Yelp do not provide an efficient platform for interactions between users in a social media environment. Furthermore, existing social media applications do not provide an efficient way for businesses and services to notify users of sales and promotional events in a localized area. FlexBook is designed to bridge this gap by providing a platform that facilitates communication between businesses and consumers. 

FlexBook’s target audience is any business or organization looking to leverage social media for marketing and promotion in a localized area, as budget conscious consumers and general social media users (13 years and older). 

## Design Choices:

### Authentication with Firebase:

In order to not only have a secure authentication feature, but also have it be reliable, firebase authentication is used. This guarantees FlexBook has the support of Google and as the number of users increase, it will be easier to make it scalable. 

### Registration with Firebase:

In designing the model for the user, the key attributes are the user’s personal information, hashtag, the user type, the profile picture path, permanent address, and the location of the user in order to make searching by location for each user possible.

### Profiles:

The layout design of the profile fragment was inspired by the user profile pages in other social media applications, such as Instagram and Twitter. The profile page displays a user’s display name and associated hashtag, a follow button, and a populated list of all posts made by that user. In the case that the selected user is a business profile, the business’ permanent address is displayed below the associated hashtag. When the fragment’s onCreateView is called, a check is run to determine if a bundle has been passed to the Profile fragment. In the case where a user is navigating to their own profile, no bundle is received, the follow button is hidden and the value of “userID” is set to the current authenticated user’s userID. In the case where a user selects a profile to navigate to, a bundle is received by the Profile fragment that contains the userID for the selected user. A query is then run using that userID to populate the profile page with that user’s information and posts. Finally, a query is run to check if the current authenticated user is following this user and update the follow button accordingly.

### Following and Unfollowing:

In order to implement following and unfollowing other users, a “Follow” button was added to the Profile fragment as well as the profile row items within the Search fragment. The User model was updated to hold a variable for userID that is passed when a user successfully follows another user. When the Follow function is called, a database check is called that queries the user’s “following” list in order to check if the selected user is currently being followed. In the case that the check returns false, the text of the button is changed to “Unfollow” and two entries are made into the database, one creating an entry into the current user’s “following” list, and one creating an entry in the selected user’s “followers” list. In the case that the check returns true, the two respective entries are deleted, and the text of the button is set to “Follow”. The list of following and followers was divided into two lists in order to facilitate querying this data from firebase as well as allow for user profiles to display these respective lists, a point discussed later in the future works section. 

### FlexFeed:

The Flexfeed is the home fragment that displays the posts by users. In order to have all the different components for each post, a custom row layout is used for each item. Colors are used to differentiate between promotional posts and normal consumer posts. Red indicates that it is a promotional post, and purple indicates it is a consumer post. The message of the post is displayed above the hashtag associated with the post, and the display name of the owner of the post is also displayed above the like and dislike image views. In order to limit the posts on the home page to posts created by users that the current user is following, the current authenticated user’s “following” list is queried to retrieve an array of userIDs. These userIds are then used to query all posts made by users whose userIDs are contained in the array. In order to allow for navigation to profiles from the FlexFeed, the home fragment was updated to navigate to the profile fragment and pass a bundle containing the selected user’s userID when a user’s profile picture is clicked. Finally, in order to ensure that the FlexFeed maintains live updates, these query functions are set to listen for Firebase events. 

### Posting:
	
Due to the fact that posting is one of the most important and common actions a user will do in the application, there is an item in the bottom navigation bar to select the post fragment. The two key input fields are the message the user wants to send out and the hashtag they want to associate the post with. Additionally, users are able to choose if the post is a promotional post by selecting or deselecting a checkbox. If it is selected, it will send a notification to users about that promotional post. In designing the model for the post, the key attributes are the user id the post was made by, the post message, the hashtag associated with the post, and one of the most important fields the location of the post in order to make searching by location possible.

### Initial Search Fragment:

The Search fragment layout was initially designed as one consolidated view containing an EditText view, search button, and two stacked lists of profiles and posts, as can be seen in screenshot F-8. When a user clicked on the search button, the text entered into the EditText view was used to query all profiles and posts with a hashtag that correlated with the entered text and update both ListViews respectively. While providing the initial basis for FlexBook’s search function, the fragment’s layout design was considerably cluttered, especially on smaller devices, and needed to be redesigned in order to provide users with an accessible interface.
Search Posts and Users Based on Location or expand globally:

The first step that was needed in order to implement searching based on location was to redesign the existing search fragment. As you can see in screenshot F-8, the initial search fragment had both profile and post searches stacked on top of each other, making it cluttered and negatively impacting the user experience. The solution was to make the search fragment responsive to the type of search the user wanted to conduct. In order to carry this out, there is a drop down spinner for the type of search the user wants, then the list view is populated based on that criteria. There is also a spinner for setting the radius of the search, the user can pick a range between 10-30 miles or expand the search globally. When the user instantiates the search function, it will first retrieve the specified radius from the spinner, followed by leveraging the helper function distanceTo(), which returns the distance between a certain post or a user and the current user’s location in meters; as a result the required radius given by the user is multiplied by 1609.34. As more accounts and posts are created, the posts and user’s search fragment are automatically updated based on the location by instantiating the startListining() method that is included in the firebase list adapter class. The startListining() method is also called in the onResume() method in order to combat cases where the user leaves the application but does not kill the activity hosting the fragment.

### Additional Search Design Decisions:

In order to allow for navigation to profiles from the Search Fragment, the fragment was updated to navigate to the Profile fragment and pass a bundle containing the selected user’s userID whenever a user’s profile picture is clicked. In addition to this, the profile ListView was updated to call the Follow and Unfollow functions & pass the selected user’s userID whenever the follo” button on a list item is clicked. Anytime the profile ListView is updated, a check is run querying the current user’s following list and updating the follow button’s text on each list item accordingly.

### Notifications of Promotional Posts:
	
In pursuance of usability, an item was added at the bottom navigation bar for notifications. One of the main things a user is going to want to look at is promotional posts in their area, having it as an item in the bottom navigation bar makes it easier to navigate directly to the notifications fragment. When designing the model for notifications, the key attributes needed were the message, hashtag associated with the message, a notification id, and most importantly the location of the notification. In order to make the notifications more relevant users only get notified in promotional posts in their local area. 
	
### Direct Messaging with Live Updates:

In order to make the UI/UX the best for direct messaging, a tab bar was implemented on top to choose between active chats and users that are available to chat. The user is also not limited to only clicking on the chat or user tab bar item in order to switch between the two fragments. There is a swipe animation added to switch between the active chats and available users fragment in order to optimize the user experience. There are also two layout files chat_item_left.xml and chat_item_right.xml in order to make the UI as realistic as any existing chat applications. In order to make this work, there was a check on each item that is added to the recyclerview in the MessageAdapter.java class. 
	
In order to follow best practices, there was a model class Chat.java for representing an individual chat. The three major attributes needed were the sender and receiver of the message as well as the message sent. In order to prevent duplicate entries, the unique id for each individual user that is generated by firebase is used for the sender and receiver fields.


## List of Implemented features and other details:

- Bottom navigation bar with home, profile, search, and post fragment
- Login with firebase
- Create a consumer account
- Create a business account
- Permanent address stored for business users
- Logout feature by killing firebase instance
- Getting and updating the user’s location
- Upload profile pictures when creating an account
- Prepopulate home screen with global posts
- Users can search profiles based on location (10, 20, 30 mile radius) 
- Users can search posts based on location (10, 20, 30 mile radius)
- Users can expand post and profile searches globally
- Make search fragment responsive to the type of search a user wants
- Spinner to select the type of search rather than having both search types displayed at once
- Users can make posts
- Users can associate posts with hashtags
- Geotagging posts using geofire
- Business users can create promotional posts
- Users are notified on promotional posts in their local area
- Direct messaging between users (live updates between messages)
- Fragment to display all users available to chat
- Fragment to display all active chats
- Fragment to send and view messages
- View and respond to messages from the chats or users fragment
- Display the username of the owner of a post in each item for the FlexFeed
- Differentiate between promotional posts and consumer posts for the FlexFeed
- Business can associate themselves with hashtags
- Users can navigate to other user’s profiles from search
- Users can navigate to other user’s profiles from FlexFeed
- Users can navigate to their own profile			
- Populate profile screen with user’s display name
- Populate profile screen with user’s posts
- Display associated hashtag on business profile
- Display permanent address on business profile
- Users can follow users
- Users can unfollow users
- Limit homepage posts to posts by users that the current user is following
- Users can run searches on posts based on hashtags
- Users can run searches on profiles based on hashtags

# External Libraries and Data Storage mechanisms:

- Firebase:
    - com.google.firebase:firebase-database:16.0.1
    - com.firebase:geofire-android:2.1.1
    - com.google.firebase:firebase-auth:16.0.1
    - com.google.firebase:firebase-storage:16.0.1
- Testing:
    - junit:junit:4.12


## Team Member Responsibilities:

### Abel Weldaregay (Features Implemented: 1 - 21):
 Abel was responsible for creating and maintaining the firebase project, as well as all design and implementation decisions regarding the following:
- Authentication with Firebase
- Registration with Firebase
- Storing user locations using Geofire
- Search fragment redesign
- Search profiles and posts by location or expand globally
- Initial FlexFeed Fragment 
- Posting
- Notifications
- Direct Messaging
- Post Fragment Design
- Notifications Fragment Design
- Login Activity Design
 In addition to this, Abel was responsible for creating and maintaining the Gitlab repository for version control and issue tracking, as well as implementing continuous integration for running remote Unit and UI Component tests.

### Amer Righi (Features Implemented: 22-34):
- Amer was responsible for all design and implementation decisions regarding the following:
- Initial Search profiles and posts by hashtag
- Additional Search Design Decisions for redesigned Search Fragment
- Associating & Displaying Business hashtag
- Navigating to User Profiles
- Displaying relevant profile information, including address & posts
- Profile Fragment Design
- Following and Unfollowing users
- Limiting FlexFeed posts by users followed
In addition to this, Amer was responsible for maintaining the Gitlab repository for version control and issue tracking.

## Future Work:

When considering ways to improve on the current state of FlexBook, upgrading and improving the general social media features within the application is an obvious place to start. Being a social media application first and foremost, it is integral to the success of FlexBook that it offers all of the common features offered by its competitors. In order to make this the case, work must be done to include the following features:
Display a List of Followers and Following on Profiles
Allow users to post images, videos, and embedded links
Allow users to like and share posts
Allow users to initiate and save group chats

In addition to these features, work must be done to improve several of FlexBook’s current features in order to truly realize the design goal of FlexBook. These include, but are not limited to, the following:
Allow Businesses to add a Date/Time stamp to Promotional Posts
Allow Businesses to Update their location and set Temporary Locations
Add an Event Calendar for User’s to track promotional posts 


## Lessons Learned:

Key lessons were learned during development in regards to optimizing our database structure in order to facilitate querying json structured data from the Firebase database.The teams approach to querying data had to be revised due to their familiarity with utilizing SQL data structures. In addition to this, java requires a large amount of boiler plate code compared to Kotlin. Taking this into consideration, it can be assumed that writing the codebase in Kotlin would have significantly shortened the length of the entire codebase.



