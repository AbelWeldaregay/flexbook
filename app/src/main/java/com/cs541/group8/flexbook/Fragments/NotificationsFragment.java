package com.cs541.group8.flexbook.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs541.group8.flexbook.Models.Notification;
import com.cs541.group8.flexbook.R;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;

public class NotificationsFragment extends Fragment {

    private ListView notificationsListView;
    private FirebaseListAdapter<Notification> adapter;
    private ArrayList<Notification> myNotifications;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_notifications, container, false);

        this.notificationsListView = view.findViewById(R.id.notificationsListView);

        this.myNotifications = new ArrayList<>();

        Query query = FirebaseDatabase.getInstance().getReference().child("Notifications");
        FirebaseListOptions<Notification> options = new FirebaseListOptions.Builder<Notification>()
                .setLayout(R.layout.notifications_row) //name of the row xml file
                .setQuery(query, Notification.class)
                .build();

        this.adapter = new FirebaseListAdapter<Notification>(options) {
            @Override
            protected void populateView(View v, Notification model, int position) {

                myNotifications.add(model);
                ((TextView) v.findViewById(R.id.notificationDescription)).setText(model.getMessage());
                ((TextView) v.findViewById(R.id.notificationMessage)).setText("#" + model.getHastag());

            }
        };

        this.notificationsListView.setAdapter(adapter);

        this.notificationsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {


                DatabaseReference drNotifications = FirebaseDatabase.getInstance().getReference("Notifications").child(myNotifications.get(position).getNotificationId());

                drNotifications.removeValue();

                Toast.makeText(getContext(), "Notification has been dismissed.", Toast.LENGTH_SHORT).show();

                return false;
            }
        });




        return view;

    }
}
