package com.cs541.group8.flexbook.Fragments;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs541.group8.flexbook.Models.Post;
import com.cs541.group8.flexbook.Models.User;
import com.cs541.group8.flexbook.R;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


public class SearchFragment extends Fragment {

    ListView searchListview;
    FirebaseListAdapter<Post> postFirebaseListAdapter;
    FirebaseListAdapter<User> userFirebaseListAdapter;
    Spinner searchTypeSpinner;
    Spinner searchDistanceSpinner;
    EditText searchEditText;
    Button searchButton;
    String selectedSearchDistance = "10 miles";
    Location lastKnownLocation = new Location("lastKnownLocation");
    LocationManager locationManager;
    LocationListener locationListener;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private DatabaseReference mDatabase;



    public void populateListView() {

        Query query = FirebaseDatabase.getInstance().getReference().child("Posts");
        FirebaseListOptions<Post> options = new FirebaseListOptions.Builder<Post>()
                .setLayout(R.layout.feed_row) //name of the row xml file
                .setQuery(query, Post.class)
                .build();




        this.postFirebaseListAdapter = new FirebaseListAdapter<Post>(options) {
            @Override
            protected void populateView(View v, final Post model, int position) {


                    ((TextView) v.findViewById(R.id.postTextView)).setText(model.getPostMessage());
                    ((TextView) v.findViewById(R.id.hashTagTextView)).setText("#" + model.getHashTag());

                    ((ImageView) v.findViewById(R.id.profilePictureImageView)).setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v){
                            //toolbar.setTitle("Profile");
                           Fragment fragment;
                           fragment = new ProfileFragment();
                           Bundle bundle = new Bundle();
                           bundle.putString("user", model.getPostByUID());
                           fragment.setArguments(bundle);
                           loadFragment(fragment);
                        }

                        private void loadFragment(Fragment fragment) {
                            // load fragment
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame_container, fragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        }
                    });

            }
        };



        postFirebaseListAdapter.startListening();
        this.searchListview.setAdapter(postFirebaseListAdapter);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this.locationListener);

            }

        }

    }


    public void populateListViewUsers() {

        Query query = FirebaseDatabase.getInstance().getReference().child("users");
        FirebaseListOptions<User> options = new FirebaseListOptions.Builder<User>()
                .setLayout(R.layout.profile_row) //name of the row xml file
                .setQuery(query, User.class)
                .build();

        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.mAuth = FirebaseAuth.getInstance();
        this.user = mAuth.getCurrentUser();




        this.userFirebaseListAdapter = new FirebaseListAdapter<User>(options) {
            @Override
            protected void populateView(View v, final User model, int position) {

                final Query query2 = FirebaseDatabase.getInstance().getReference().child("Followers").child(model.getImageUri()).child(user.getUid());

                ((TextView) v.findViewById(R.id.nameTextView)).setText(model.getFirstName() + " " + model.getLastName());
                ((TextView) v.findViewById(R.id.profileHashTagTextView)).setText("#" + model.getHashtag());

                final Button btnFollow = v.findViewById(R.id.btnFollow);
                final String Uid = model.getImageUri();

                final ValueEventListener eventListener2 = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()) {
                            btnFollow.setText("Unfollow");
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                };
                final Query query3 = FirebaseDatabase.getInstance().getReference().child("Followers").child(model.getUserId()).child(user.getUid());
                query3.addListenerForSingleValueEvent(eventListener2);

                final String currentUser = user.getUid();

                final ValueEventListener eventListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(!dataSnapshot.exists()) {
                            mDatabase.child("Followers").child(Uid).child(currentUser).setValue(currentUser);
                            mDatabase.child("Following").child(currentUser).child(Uid).setValue(Uid);
                            btnFollow.setText("Unfollow");
                        }
                        else {
                            mDatabase.child("Followers").child(Uid).child(currentUser).removeValue();
                            mDatabase.child("Following").child(currentUser).child(Uid).removeValue();
                            btnFollow.setText("Follow");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                };

                ((ImageView) v.findViewById(R.id.profilePictureImageView1)).setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        //toolbar.setTitle("Profile");
                        Fragment fragment;
                        fragment = new ProfileFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("user", model.getImageUri());
                        fragment.setArguments(bundle);
                        loadFragment(fragment);
                    }

                    private void loadFragment(Fragment fragment) {
                        // load fragment
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_container, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                });



                ((Button) v.findViewById(R.id.btnFollow)).setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {



                        //mDatabase.child("Followers").child(Uid).child(currentUser).setValue(true);
                        //mDatabase.child("Following").child(currentUser).child(Uid).setValue(true);
                        query2.addListenerForSingleValueEvent(eventListener);
                    }
                });
            }
        };

        userFirebaseListAdapter.startListening();
        this.searchListview.setAdapter(userFirebaseListAdapter);

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_search, container, false);

        this.searchListview = view.findViewById(R.id.searchListView);
        this.searchTypeSpinner = view.findViewById(R.id.searchTypeSpinner);
        this.searchEditText = view.findViewById(R.id.searchEditText);
        this.searchButton = view.findViewById(R.id.searchButton);
        this.searchDistanceSpinner = view.findViewById(R.id.searchDistanceSpinner);
        this.locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);


        this.searchTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(searchTypeSpinner.getSelectedItem().toString().equals("Search by Post #")) {

                    searchEditText.setHint(searchTypeSpinner.getSelectedItem().toString());
                    populateListView();
                } else {
                    searchEditText.setHint(searchTypeSpinner.getSelectedItem().toString());
                    populateListViewUsers();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        this.searchDistanceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedSearchDistance = searchDistanceSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        this.locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("NEW LOCATION FROM SEARCH:  ", location.toString());
                lastKnownLocation = location;

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };


        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);


        } else{

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this.locationListener);
        }


        this.searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String searchValue = searchEditText.getText().toString();


                if(searchTypeSpinner.getSelectedItem().toString().equals("Search by Post #")) {



                    // search by posts hash tag
                    Query postsQuery = FirebaseDatabase.getInstance().getReference().child("Posts").orderByChild("hashTag").startAt(searchValue).endAt(searchValue + "\uf8ff");

                    FirebaseListOptions<Post> postOptions = new FirebaseListOptions.Builder<Post>()
                            .setLayout(R.layout.feed_row)
                            .setQuery(postsQuery, Post.class)
                            .build();



                    postFirebaseListAdapter = new FirebaseListAdapter<Post>(postOptions) {


                        @Override
                        protected void populateView(View v, Post model, int position) {


                            Location tempLocation = new Location("tempLocation");

                            tempLocation.setLongitude(model.getLongitude());
                            tempLocation.setLatitude(model.getLatitude());


                            double requiredDistance = 1609.34;
                            if(searchDistanceSpinner.getSelectedItem().equals("10 miles")){
                                requiredDistance = requiredDistance * 10;
                            } else if(searchDistanceSpinner.getSelectedItem().equals("20 miles")){
                                requiredDistance = requiredDistance * 20;
                            } else if(searchDistanceSpinner.getSelectedItem().equals("30 miles")){
                                requiredDistance = requiredDistance * 30;
                            } else if(searchDistanceSpinner.getSelectedItem().equals("Global")) {
                                requiredDistance = 0.0;
                            }

                            if(requiredDistance != 0.0) {


                                if(lastKnownLocation.distanceTo(tempLocation) <=  requiredDistance) {

                                    ((ImageView) v.findViewById(R.id.profilePictureImageView)).setVisibility(View.VISIBLE);
                                    ((ImageButton) v.findViewById(R.id.likeButton)).setVisibility(View.VISIBLE);
                                    ((ImageButton) v.findViewById(R.id.shareButton)).setVisibility(View.VISIBLE);
                                    ((TextView) v.findViewById(R.id.postTextView)).setVisibility(View.VISIBLE);

                                    ((TextView) v.findViewById(R.id.hashTagTextView)).setVisibility(View.VISIBLE);
                                    ((TextView) v.findViewById(R.id.postTextView)).setText(model.getPostMessage());

                                    ((TextView) v.findViewById(R.id.hashTagTextView)).setText("#" + model.getHashTag());

                                }
                                else {

                                    ((ImageView) v.findViewById(R.id.profilePictureImageView)).setVisibility(View.GONE);
                                    ((ImageButton) v.findViewById(R.id.likeButton)).setVisibility(View.GONE);
                                    ((ImageButton) v.findViewById(R.id.shareButton)).setVisibility(View.GONE);

                                    ((TextView) v.findViewById(R.id.postTextView)).setVisibility(View.GONE);

                                    ((TextView) v.findViewById(R.id.hashTagTextView)).setVisibility(View.GONE);

                                }


                            } else {

                              //  ((ImageView) v.findViewById(R.id.profilePictureImageView)).setVisibility(View.VISIBLE);
                                ((TextView) v.findViewById(R.id.postTextView)).setText(model.getPostMessage());
                                ((TextView) v.findViewById(R.id.hashTagTextView)).setText("#" + model.getHashTag());


                            }



                        }
                    };

                    searchListview.setAdapter(postFirebaseListAdapter);
                    postFirebaseListAdapter.startListening();

                } else {

                    Query userQuery = FirebaseDatabase.getInstance().getReference().child("users").orderByChild("hashtag").startAt(searchValue).endAt(searchValue + "\uf8ff");

                    FirebaseListOptions<User> userOptions = new FirebaseListOptions.Builder<User>()
                            .setLayout(R.layout.profile_row)
                            .setQuery(userQuery, User.class)
                            .build();

                    userFirebaseListAdapter = new FirebaseListAdapter<User>(userOptions) {
                        @Override
                        protected void populateView(View v, User model, int position) {

                            Location tempLocation = new Location("tempLocation");

                            tempLocation.setLongitude(model.getLongitude());
                            tempLocation.setLatitude(model.getLatitude());

                            double requiredDistance = 1609.34;
                            if(searchDistanceSpinner.getSelectedItem().equals("10 miles")){
                                requiredDistance = requiredDistance * 10;
                            } else if(searchDistanceSpinner.getSelectedItem().equals("20 miles")){
                                requiredDistance = requiredDistance * 20;
                            } else if(searchDistanceSpinner.getSelectedItem().equals("30 miles")){
                                requiredDistance = requiredDistance * 30;
                            }
                            else if(searchDistanceSpinner.getSelectedItem().equals("Global")) {
                                requiredDistance = 0.0;
                            }

                            if(requiredDistance != 0.0) {

                                if(lastKnownLocation.distanceTo(tempLocation) <= requiredDistance) {

                                    ((ImageView) v.findViewById(R.id.profilePictureImageView1)).setVisibility(View.VISIBLE);
                                    ((TextView) v.findViewById(R.id.nameTextView)).setVisibility(View.VISIBLE);
                                    ((TextView) v.findViewById(R.id.profileHashTagTextView)).setVisibility(View.VISIBLE);
                                    ((Button) v.findViewById(R.id.btnFollow)).setVisibility(View.VISIBLE);
                                    ((TextView) v.findViewById(R.id.nameTextView)).setText(model.getFirstName() + " " + model.getLastName());
                                    ((TextView) v.findViewById(R.id.profileHashTagTextView)).setText("#" + model.getHashtag());


                                } else {

                                    ((ImageView) v.findViewById(R.id.profilePictureImageView1)).setVisibility(View.GONE);
                                    ((TextView) v.findViewById(R.id.nameTextView)).setVisibility(View.GONE);
                                    ((TextView) v.findViewById(R.id.profileHashTagTextView)).setVisibility(View.GONE);
                                    ((Button) v.findViewById(R.id.btnFollow)).setVisibility(View.GONE);


                                }

                            } else {
                                ((TextView) v.findViewById(R.id.nameTextView)).setText(model.getFirstName() + " " + model.getLastName());
                                ((TextView) v.findViewById(R.id.profileHashTagTextView)).setText("#" + model.getHashtag());


                            }

                        }
                    };


                    searchListview.setAdapter(userFirebaseListAdapter);
                    userFirebaseListAdapter.startListening();



                }

            }
        });


        return view;

    }


}
