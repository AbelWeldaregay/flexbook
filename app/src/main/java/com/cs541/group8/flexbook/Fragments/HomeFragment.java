package com.cs541.group8.flexbook.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs541.group8.flexbook.Models.Post;
import com.cs541.group8.flexbook.R;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

public class HomeFragment extends Fragment {

    LocationManager locationManager;
    LocationListener locationListener;
    Location mLastLocation;
    ListView feedListView;
    FirebaseListAdapter<Post> adapter;
    static HomeFragment instance;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private FirebaseDatabase mDb;
    private DatabaseReference mRef;
    String postUid;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this.locationListener);

            }

        }

    }

    public static HomeFragment getInstance() {
        return instance;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        instance = this;
        this.locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        this.feedListView = view.findViewById(R.id.feedListView);
        mAuth = FirebaseAuth.getInstance();
        mDb = FirebaseDatabase.getInstance();
        mRef = mDb.getReference();
        this.user = mAuth.getCurrentUser();


        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReferenceFromUrl("gs://flexbook-1544c.appspot.com").child(mAuth.getUid()+ ".png");


        mRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot childDataSnapshot : dataSnapshot.child("Following").child(user.getUid()).getChildren()) {


                postUid = childDataSnapshot.getKey();
                //Log.d(TAG, "showData: MyUid: " + user.getUid());
                Log.d(TAG, "showData: Uid: " + postUid);

                    Query query = FirebaseDatabase.getInstance().getReference().child("Posts").orderByChild("postByUID").startAt(postUid).endAt(postUid + "\uf8ff");
                    FirebaseListOptions<Post> options = new FirebaseListOptions.Builder<Post>()
                            .setLayout(R.layout.feed_row) //name of the row xml file
                            .setQuery(query, Post.class)
                            .build();


                    adapter = new FirebaseListAdapter<Post>(options) {
                        @Override
                        protected void populateView(View v, final Post model, int position) {

                            if(model.isBusinessPost()) {
                                ((TextView) v.findViewById(R.id.postByTextView)).setTextColor(getResources().getColor(R.color.red));
                                ((TextView) v.findViewById(R.id.postTextView)).setTextColor(getResources().getColor(R.color.red));
                                ((TextView) v.findViewById(R.id.hashTagTextView)).setTextColor(getResources().getColor(R.color.red));

                            } else {
                                ((TextView) v.findViewById(R.id.postByTextView)).setTextColor(getResources().getColor(R.color.purple));
                                ((TextView) v.findViewById(R.id.postTextView)).setTextColor(getResources().getColor(R.color.purple));
                                ((TextView) v.findViewById(R.id.hashTagTextView)).setTextColor(getResources().getColor(R.color.purple));

                            }

                            ((TextView) v.findViewById(R.id.postByTextView)).setText(model.getPostByUserName());
                            ((TextView) v.findViewById(R.id.postTextView)).setText(model.getPostMessage());
                            ((TextView) v.findViewById(R.id.hashTagTextView)).setText("#" + model.getHashTag());

                            ((ImageView) v.findViewById(R.id.profilePictureImageView)).setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    //toolbar.setTitle("Profile");
                                    Fragment fragment;
                                    fragment = new ProfileFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("user", model.getPostByUID());
                                    fragment.setArguments(bundle);
                                    loadFragment(fragment);
                                }

                                private void loadFragment(Fragment fragment) {
                                    // load fragment
                                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                    transaction.replace(R.id.frame_container, fragment);
                                    transaction.addToBackStack(null);
                                    transaction.commit();
                                }
                            });

                        }
                    };
                    feedListView.setAdapter(adapter);
                    adapter.startListening();


             //   }



            }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });





        this.locationListener =  new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                Log.i("Location: ", location.toString());
                mLastLocation = location;

                String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("UsersLoggedIn");

                GeoFire geoFire = new GeoFire(ref);
                geoFire.setLocation(userId, new GeoLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude()));


            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            //ask for permission
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
            //we have permission
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, this.locationListener);
        }



        return view;

    }


}
