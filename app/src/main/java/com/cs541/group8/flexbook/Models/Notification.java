package com.cs541.group8.flexbook.Models;

import android.location.Location;

public class Notification {

    String message;
    String hashTag;
    Location location;
    String notificationId;

    public void setHashTag(String hashTag) {
        this.hashTag = hashTag;
    }

    public Notification(){}

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setHastag(String hashTag) {
        this.hashTag = hashTag;
    }

    public String getMessage() {
        return message;
    }

    public String getHastag() {
        return this.hashTag;
    }

    public Notification(String message, String hashTag, String notificationId) {
        this.message = message;
        this.hashTag = hashTag;
        this.notificationId = notificationId;
    }
}
