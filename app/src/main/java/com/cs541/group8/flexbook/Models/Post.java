package com.cs541.group8.flexbook.Models;

import android.location.Location;

public class Post {

    private String postByUID;
    private String postId;
    private String postMessage;
    private String hashTag;
    private boolean businessPost;
    private String postByUserName;

    public String getPostByUserName() {
        return postByUserName;
    }

    public void setPostByUserName(String postByUserName) {
        this.postByUserName = postByUserName;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    Double  latitude;
    Double  longitude;

    public void setPostId(String postId) {
        this.postId = postId;
    }


    public void setHashTag(String hashTag) {
        this.hashTag = hashTag;
    }

    public String getPostId() {
        return postId;
    }

    public Post(){}

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Post(String displayName, String postByUID, String postId, String postMessage, String hashTag, boolean businessPost, Double latitude, Double longitude) {

        this.postByUID = postByUID;
        this.postId = postId;
        this.postMessage = postMessage;
        this.hashTag = hashTag;
        this.businessPost = businessPost;
        this.latitude = latitude;
        this.longitude = longitude;
        this.postByUserName = displayName;
    }


    public void setBusinessPost(boolean businessPost) {
        this.businessPost = businessPost;
    }

    public String getHashTag() {
        return hashTag;
    }

    public boolean isBusinessPost() {
        return businessPost;
    }

    public void setPostByUID(String postByUID) {
        this.postByUID = postByUID;
    }

    public void setPostMessage(String postMessage) {
        this.postMessage = postMessage;
    }

    public String getPostByUID() {
        return postByUID;
    }

    public String getPostMessage() {
        return postMessage;
    }

}
