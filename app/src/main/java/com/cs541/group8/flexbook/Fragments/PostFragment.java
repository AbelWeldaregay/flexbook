package com.cs541.group8.flexbook.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.cs541.group8.flexbook.Models.Notification;
import com.cs541.group8.flexbook.Models.Post;
import com.cs541.group8.flexbook.R;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.UUID;

public class PostFragment extends Fragment {


    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private EditText postEditText;
    private Button postButton;
    private CheckBox checkBox;
    private DatabaseReference mDatabase;
    private EditText hashTagEditText;
    LocationManager locationManager;
    LocationListener locationListener;
    Location mLastLocation;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 0, 0, this.locationListener);

            }

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_post, container, false);

        this.mAuth = FirebaseAuth.getInstance();
        this.user = mAuth.getCurrentUser();
        this.postEditText = view.findViewById(R.id.postEditText);
        this.postButton = view.findViewById(R.id.postButton);
        this.checkBox = view.findViewById(R.id.isBusinessPostCheckBox);
        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.hashTagEditText = view.findViewById(R.id.hashTagEditText);

        this.locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        this.locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.i("Location: ", location.toString());
                mLastLocation = location;

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //ask for permission
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            // we have permission
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, this.locationListener);

        }


        this.postButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                postButton.setEnabled(false);
                String postMessage = postEditText.getText().toString();
                Boolean isBusinessPost = checkBox.isChecked();
                String hasTag = hashTagEditText.getText().toString();
                postMessage.trim();

                Double longitude = mLastLocation.getLongitude();
                Double latitude = mLastLocation.getLatitude();

                if(!postMessage.isEmpty()) {

                    Post newPost = new Post(user.getDisplayName(), user.getUid(), UUID.randomUUID().toString(), postMessage, hasTag, isBusinessPost, latitude, longitude );

                    mDatabase.child("Posts").child(newPost.getPostId()).setValue(newPost);

                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("PostLocation");
                    GeoFire geoFire = new GeoFire(ref);
                    geoFire.setLocation(newPost.getPostId(), new GeoLocation(HomeFragment.getInstance().mLastLocation.getLatitude(), HomeFragment.getInstance().mLastLocation.getLongitude()));

                    if(checkBox.isChecked()) {

                        Notification notification = new Notification(postMessage, hasTag, UUID.randomUUID().toString());
                        mDatabase.child("Notifications").child(notification.getNotificationId()).setValue(notification);

                    }



                    postEditText.setText("");
                    checkBox.setChecked(false);
                    hashTagEditText.setText("");
                    postButton.setEnabled(true);

                } else {

                    Toast.makeText(getContext(), "Post Cannot be empty, Please try again.", Toast.LENGTH_SHORT).show();

                    postButton.setEnabled(true);


                }

            }
        });





        return view;

    }
}
