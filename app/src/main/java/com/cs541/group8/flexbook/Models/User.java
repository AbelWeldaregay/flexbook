package com.cs541.group8.flexbook.Models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

/**
 * Represents a User
 */
@IgnoreExtraProperties
public class User implements Serializable {

    private String firstName;
    private String lastName;
    private String userType;
    private String displayName;
    private String email;
    private String userId;
    private String hashtag;
    private String imageUri;
    private double longitude;
    private double latitude;
    private String imageDownloadUrl;
    private String perminantAddress;

    public void setPerminantAddress(String perminantAddress) {
        this.perminantAddress = perminantAddress;
    }

    public String getPerminantAddress() {
        return perminantAddress;
    }

    public void setImageDownloadUrl(String imageDownloadUrl) {
        this.imageDownloadUrl = imageDownloadUrl;
    }

    public String getImageDownloadUrl() {
        return imageDownloadUrl;
    }

    public User(){};

    public User(String userId, String firstName, String lastName, String userType, String displayName, String email, String hashtag, String imageUri, double latitude, double longitude) {


        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userType = userType;
        this.displayName = displayName;
        this.email = email;
        this.hashtag = hashtag;
        this.imageUri = imageUri;
        this.longitude = longitude;
        this.latitude = latitude;
        this.imageDownloadUrl = imageDownloadUrl;

    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUserType() {
        return userType;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getEmail() {
        return email;
    }

    public String getHashtag() {
        return hashtag;
    }

    public String getImageUri() {
        return imageUri;
    }
}
