package com.cs541.group8.flexbook.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.cs541.group8.flexbook.R;
import com.cs541.group8.flexbook.Models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText emailEditText;
    private EditText passwordEditText;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseApp.initializeApp(this);

        this.mAuth = FirebaseAuth.getInstance();
        this.mDatabase = FirebaseDatabase.getInstance().getReference();


        this.emailEditText = findViewById(R.id.emailEditText);
        this.passwordEditText = findViewById(R.id.passwordEditText);

    }

    /**
     * Login User
     * @param view
     */
    public void loginAction(View view){

        String possibleEmail = this.emailEditText.getText().toString();
        String possiblePassword = this.passwordEditText.getText().toString();

        if(possibleEmail.isEmpty() || possibleEmail == null) {

            Toast.makeText(LoginActivity.this, "Email cannot be empty. Try again", Toast.LENGTH_LONG).show();

        } else if(possiblePassword.isEmpty() || possibleEmail == null) {

            Toast.makeText(LoginActivity.this, "Password cannot be empty. Try again", Toast.LENGTH_LONG).show();

        } else {

            mAuth.signInWithEmailAndPassword(possibleEmail, possiblePassword)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("loginProcess", "signInWithEmail:success");
                                Toast.makeText(LoginActivity.this, "login success.",
                                        Toast.LENGTH_SHORT).show();
                                FirebaseUser user = mAuth.getCurrentUser();
                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                intent.putExtra("userLoggedIn", user.getUid());
                                startActivity(intent);
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("loginProcess", "signInWithEmail:failure", task.getException());
                                Toast.makeText(LoginActivity.this, task.getException().toString(),
                                        Toast.LENGTH_SHORT).show();

                                // updateUI(null);

                            }

                        }
                    });

        }

    }

    public void updateUI(User user){

    }
    public void signUpOnClick(View view){
        // TODO
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        LoginActivity.this.startActivity(intent);

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

    }
}
