package com.cs541.group8.flexbook.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.cs541.group8.flexbook.Models.Post;
import com.cs541.group8.flexbook.Models.User;
import com.cs541.group8.flexbook.R;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class ProfileFragment extends Fragment {

    LocationManager locationManager;
    LocationListener locationListener;
    Location mLastLocation;
    ListView profListView;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    FirebaseListAdapter<User> adapter;
    FirebaseListAdapter<Post> adapter2;
    private DatabaseReference mDb;
    TextView profName;
    TextView profHashtag;
    TextView profAddy;
    ImageView profilePicture;
    String userID;
    Button btnFollow;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this.locationListener);

            }

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        adapter2.startListening();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_profile, container, false);





        this.mAuth = FirebaseAuth.getInstance();
        this.user = mAuth.getCurrentUser();

        this.locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        this.profListView = view.findViewById(R.id.profListView);

        this.profName = view.findViewById(R.id.profName);
        this.profHashtag = view.findViewById(R.id.profHashtag);
        this.profAddy = view.findViewById(R.id.profAddy);
        this.profilePicture = view.findViewById(R.id.profilePicture);
        this.btnFollow = view.findViewById(R.id.btnFollow);
        this.mDb = FirebaseDatabase.getInstance().getReference();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            this.userID = bundle.getString("user");
            //this.userID = this.user.getUid();


            final ValueEventListener eventListener2 = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()) {
                       btnFollow.setText("Unfollow");
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            final Query query2 = FirebaseDatabase.getInstance().getReference().child("Followers").child(this.userID).child(user.getUid());
            query2.addListenerForSingleValueEvent(eventListener2);


        }
        else {
            this.userID = this.user.getUid();
            this.btnFollow.setVisibility(View.GONE);
        }
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users").child(this.userID);
        final Query query2 = FirebaseDatabase.getInstance().getReference().child("Followers").child(this.userID).child(user.getUid());

        final String Uid = this.userID;

        final String currentUser = user.getUid();

        final ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()) {
                    mDb.child("Followers").child(Uid).child(currentUser).setValue(currentUser);
                    mDb.child("Following").child(currentUser).child(Uid).setValue(Uid);
                    btnFollow.setText("Unfollow");
                }
                else {
                    mDb.child("Followers").child(Uid).child(currentUser).removeValue();
                    mDb.child("Following").child(currentUser).child(Uid).removeValue();
                    btnFollow.setText("Follow");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        btnFollow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                query2.addListenerForSingleValueEvent(eventListener);
            }
        });


        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                //Log.d(TAG, "Value is: " + value);
                String name = dataSnapshot.child("displayName").getValue(String.class);
                String tag = dataSnapshot.child("hashtag").getValue(String.class);
                String pic = dataSnapshot.child("imageUri").getValue(String.class);
                String addy = dataSnapshot.child("perminantAddress").getValue(String.class);
                profName.setText(name);
                profHashtag.setText(tag);
                profAddy.setText(addy);
                //profilePicture.setImageURI(Uri.parse(pic));

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Query query = FirebaseDatabase.getInstance().getReference().child("Posts").orderByChild("postByUID").startAt(this.userID).endAt(this.userID + "\uf8ff");
        FirebaseListOptions<Post> options = new FirebaseListOptions.Builder<Post>()
                .setLayout(R.layout.feed_row) //name of the row xml file
                .setQuery(query, Post.class)
                .build();


        this.adapter2 = new FirebaseListAdapter<Post>(options) {
            @Override
            protected void populateView(View v, Post model, int position) {

                ((TextView) v.findViewById(R.id.postTextView)).setText(model.getPostMessage());
                ((TextView) v.findViewById(R.id.hashTagTextView)).setText("#" + model.getHashTag());
            }
        };

        this.profListView.setAdapter(adapter2);





        this.locationListener =  new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                Log.i("Location: ", location.toString());
                mLastLocation = location;

                String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("UsersLoggedIn");

                GeoFire geoFire = new GeoFire(ref);
                geoFire.setLocation(userId, new GeoLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude()));


            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            //ask for permission
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
            //we have permission
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, this.locationListener);
        }



        // Or Create a reference to a file from a Google Cloud Storage URI
//        StorageReference gsReference =
//                storage.getReferenceFromUrl("gs://flexbook-1544c.appspot.com/displayPics/" + mAuth.getUid());


        FirebaseStorage storage = FirebaseStorage.getInstance();

        StorageReference storageReference = storage.getReference();

        StorageReference imagesRef = storageReference.child("displayPics");

        StorageReference spaceRef = storageReference.child("displayPics/" + mAuth.getCurrentUser().getUid());


//
//        gsReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//            @Override
//            public void onSuccess(Uri uri) {
//                profilePicture.setImageURI(uri);
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(getContext(), "failed to download image", Toast.LENGTH_SHORT).show();
//            }
//        });



        return view;

    }


}
