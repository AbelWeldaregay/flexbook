package com.cs541.group8.flexbook.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs541.group8.flexbook.Models.Post;
import com.cs541.group8.flexbook.R;

import java.util.ArrayList;

public class PostAdapter extends ArrayAdapter<Post> {

    public static final String TAG = "POST_ADAPTER";
    private Context mContext;
    private int mResource;


    /**
     * Holds contents of a view
     */
    static class ViewHolder {
        ImageView imageView;
        TextView postTextView;
        TextView hashTagTextView;
    }


    /**
     *
     * @param context
     * @param resource
     * @param objects
     */
    public PostAdapter(Context context, int resource, ArrayList<Post> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;

    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String postMessage = getItem(position).getPostMessage();
        String postHashtag = getItem(position).getHashTag();

        ViewHolder holder;

        if(convertView == null) {

            LayoutInflater inflater = LayoutInflater.from(this.mContext);
            convertView = inflater.inflate(this.mResource, parent, false);

            holder = new ViewHolder();
            holder.postTextView = convertView.findViewById(R.id.postTextView);
            holder.hashTagTextView = convertView.findViewById(R.id.hashTagTextView);

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        final Integer index = new Integer(position);

        holder.hashTagTextView.setText(postHashtag);
        holder.postTextView.setText(postMessage);

        return convertView;

    }


}
